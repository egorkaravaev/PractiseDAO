package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ConnectionPool {
    private int size;
    private int minPoolSize;
    private int maxPoolSize;
    private long idleTimeLimit;
    private double loadFactor;
    private int incrementPortion;

    final String DB_DRIVER = "com.mysql.jdbc.Driver";
    final String DB_URL = "jdbc:mysql://localhost:3306/Books";
    final String DB_USERNAME = "root";
    final String DB_PASSWORD = "root";

    private LinkedList<WrapperConnection> wrapperConnections = new LinkedList<WrapperConnection>();

    public ConnectionPool() {
    }

    public ConnectionPool(Configuration configuration) {
        try {
            Class.forName(DB_DRIVER);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.minPoolSize = configuration.getMinPoolSize();
        this.size = minPoolSize;
        this.maxPoolSize = configuration.getMaxPoolSize();
        this.idleTimeLimit = configuration.getIdleTimeLimit();
        this.loadFactor = configuration.getLoadFactor();
        this.incrementPortion = configuration.getIncrementPortion();
        addConnections(minPoolSize);
    }

    private void addConnections(int quantity) {
        ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() + 1);
        for(int i = 0; i < quantity; i++) {
            try {
                wrapperConnections.add(executorService.submit(new ConnectionCallable()).get());
            } catch (Exception e) {
                System.err.println("Connection error" + e);
            }
        }
        executorService.shutdown();
    }

    private class ConnectionCallable implements Callable<WrapperConnection> {
        public WrapperConnection call() throws Exception {
            return new WrapperConnection(getConnection());
        }
    }

    private Connection getConnection() {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(DB_URL,DB_USERNAME,DB_PASSWORD);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conn;
    }

    public synchronized Connection retrieve() throws SQLException {
        Connection newConn;
        WrapperConnection wc;
        if(size <= maxPoolSize) {
            if (wrapperConnections.size() <= loadFactor / 100 * size) {
                addConnections(incrementPortion);
                size = size + incrementPortion;

                wc = wrapperConnections.getLast();
                newConn = wc.getConnection();
                wrapperConnections.removeLast();
            } else {
                wc = wrapperConnections.getLast();
                newConn = wc.getConnection();
                wrapperConnections.removeLast();
            }

            return newConn;
        } else {
            System.out.println("The pool size is exceeded!");
            return null;
        }
    }

    public synchronized void putBack(Connection c){
        WrapperConnection wc = new WrapperConnection(c);
        wrapperConnections.add(wc);
    }

    public LinkedList<WrapperConnection> getWrapperConnections() {
        return wrapperConnections;
    }

    public long getIdleTimeLimit() {
        return idleTimeLimit;
    }
}
