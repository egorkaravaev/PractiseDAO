package util;


import java.util.Iterator;

public class UtilThread extends Thread {
    ConnectionPool connectionPool = new ConnectionPool();


    private boolean isActive;

    public UtilThread() {
        isActive = true;
    }

    @Override
    public void run() {
        try {
            while (isActive) {
                if (isDaemon()) {
                    Iterator<WrapperConnection> iter = connectionPool.getWrapperConnections().iterator();
                    long currentTime = System.currentTimeMillis();
                    while (iter.hasNext()) {
                        if (currentTime - iter.next().getCreationTime() >= connectionPool.getIdleTimeLimit()) {
                            connectionPool.getWrapperConnections().remove(iter);
                            System.out.println("connection was delited");
                        }
                    }
                    Thread.sleep(10000);
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void disable(){
        isActive = false;
    }
}
