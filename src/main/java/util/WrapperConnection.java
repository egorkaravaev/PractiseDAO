package util;

import java.sql.Connection;

public class WrapperConnection {
    private Connection connection;
    private long creationTime = System.currentTimeMillis();

    public WrapperConnection(Connection connection) {
        this.connection = connection;
    }

    public Connection getConnection() {
        return connection;
    }

    public long getCreationTime() {
        return creationTime;
    }
}
